package tasks;

import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import res.Location;
import res.Settings;

public class Pick extends Task {


    /** getID() != # has unexpected behaviour & doesn't work for some strange reason - instead
     * it clicks on empty bushes instead of world hopping
     * private final String BUSH = "bush";
     * private final Predicate<SceneObject> BUSH_PREDICATE = b ->
     *     (b.getId() != 23630 || b.getId() != 23627)
     *     && b.getName().contains(BUSH);
     */
    // Pick from action string because hard coded strings are for scrubs.
    private final String ACTION = "Pick-from";
    // Object instance of the local player
    private Player local = Players.getLocal();

    @Override
    public boolean validate() {
        // Update the local player instance before validating, otherwise unexpected behaviour
        local = Players.getLocal();

        // If the players inventory is full and the player is in the BUSHES area defined in Location.java
        // and the local player is not moving and the nearest valid berry bush does not exist (they have all
        // been picked and are waiting to respawn) then return true and execute this task
        return Settings.start
                && !Inventory.isFull()
                && Location.BUSHES.getArea().contains(local)
                && !local.isMoving()
                && SceneObjects.getNearest(Settings.BUSH) != null;
    }

    @Override
    public int execute() {
        //TODO:Improve the paint pick up algorithm
        //Get the nearest valid bush using the predicate defined in Settings.java
        SceneObject bush = SceneObjects.getNearest(Settings.BUSH);
        //Get the name of the bush for use in the Paint
        //Always null check entities, you can't use bush.interact() on a null object
        if(bush != null){
            //Update paint variable
            String berry = bush.getName();
            //Used for updating picked variable
            Settings.lastBerry = berry;
            Settings.state = "Foraging [" + berry + "]";
            //Check method below
            checkInventory(berry);
            //Updates the tracked count variable
            Settings.inventoryCount = Inventory.getCount();
            //Interact with the berry bush using the ACTION string from above ("Pick-from"), if clicked then...
            if(bush.interact(ACTION)){
                //Sleep until the player's inventory count goes up, meaning a berry has been picked
                //check the condition every 50ms for 1 second.
                Time.sleepUntil(() -> Inventory.getCount() != Settings.inventoryCount, 50, 1000);
            }
        }
        //The script sleeps for 250ms after the task finishes execution
        return 250;
    }

    private void checkInventory(String berry){
        //If the current inventory count doesn't equal the saved data then...
        if(Inventory.getCount() != Settings.inventoryCount){
            //Increment the paint counter
            Settings.picked++;
            //Increment the berry counter used to accurately calculate profit when picking both berries
            if(berry.contains("Red")){
                Settings.redberry++;
            }
            else{
                Settings.cadava++;
            }
        }
    }
}
