package tasks;

import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import res.Location;
import res.Settings;

public class Traverse extends Task {

    // Object instance of the local player
    private Player local = Players.getLocal();

    @Override
    public boolean validate() {
        // Update the local player instance before validating, otherwise unexpected behaviour
        local = Players.getLocal();
        // Returns true if the either bank() or bush() returns true
        // Check the bank() and bush() methods below
        return Settings.start
                && (bank() || bush());
    }

    @Override
    public int execute() {

        // Get an int between [70, 100) [Inclusive of 70, exclusive of 100]
        // If the player's run energy is above the randomly generated number then...
        if(Movement.getRunEnergy() > Settings.energyActivation){
            // if run is not enabled then...
            if(!Movement.isRunEnabled()){
                // Enable run
                Movement.toggleRun(true);
                //Get a new random number for next run activation
                Settings.energyActivation = org.rspeer.runetek.api.commons.math.Random.nextInt(70,100);
            }
        }

        //Check the inventory count in case the last check was missed because of a full inventory
        checkInventory(Settings.lastBerry);

        /** 1) **/
        // Movement.walkTo() is used to walk to either the bank or berries.
        /** 2) **/
        // The argument for the walkTo call is called a ternary operator
        // Basically, a ternary operator means if x is true then do y
        // if x is false, then do z. In Java it looks like this: x ? y : z;
        // In this case: if bank() is true, then the argument of walkTo
        // will be Location.BANK.getArea().getCenter(). If bank() is false,
        // then the argument will be Location.BERRY_AREA.getArea().getCenter().
        // x = bank(), y = Location.BANK... z = Location.BERRY_AREA...
        Movement.walkTo(bank() ? Location.BANK.getArea().getCenter() : Location.BERRY_AREA.getArea().getCenter());

        // Sleep Until the local player's distance to the walking destination is less than 12
        // Check the condition every 50ms for 5 seconds
        Time.sleepUntil(() -> local.distance(Movement.getDestination()) < 12, 50, 5000);

        //the script sleeps for 250ms after task execution
        return 250;
    }

    // If the players inventory is full then bank() returns true, update the paint variable Settings.state
    private boolean bank(){
        if(Inventory.isFull()){
            Settings.state = "Walking to Bank";
            return true;
        }
        return false;
    }

    // If the players inventory is empty and they are not in the bush area defined in Location.java then
    // bush() returns true, update the paint variable Settings.state
    private boolean bush(){
        if(Inventory.isEmpty() && !Location.BUSHES.getArea().contains(local)){
            Settings.state = "Walking to Berries";
            return true;
        }
        return false;
    }

    //Check the description in Pick.java for more info
    //Used to increment counter when last berry is picked and inventory is full
    private void checkInventory(String berry){
        if(Inventory.getCount() != Settings.inventoryCount){
            Settings.picked++;
            if(berry.contains("Red")){
                Settings.redberry++;
            }
            else{
                Settings.cadava++;
            }
            Settings.inventoryCount = Inventory.getCount();
        }
    }

}
