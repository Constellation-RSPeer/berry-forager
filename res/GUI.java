package res;

import org.rspeer.runetek.api.ClientSupplier;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

    private JPanel panel;
    private JCheckBox reds, cads;
    private JButton start;
    private JLabel title;

    public GUI(){
        //Set the title of the JFrame
        super("-Forager-");

        //Initialize the panel that will hold the checkboxes and button
        panel = new JPanel();

        //Initialize JLabel and JCheckBoxes
        title = new JLabel("Pick Berries:");
        reds = new JCheckBox("Redberry");
        cads = new JCheckBox("Cadava Berry");
        start = new JButton("Start Script");

        //Set the location of the frame relative to RSPeer, so the frame doesn't
        //Open in the top left corner of the monitor
        setLocationRelativeTo(ClientSupplier.get().getCanvas());
        //setSize(new Dimension(450, 95));
        panel.add(title);
        panel.add(reds);
        panel.add(cads);
        panel.add(start);
        add(panel);
        //Pack all the components together and trim extra space
        pack();

        setDefaultCloseOperation(HIDE_ON_CLOSE);

        //Add action listener to start script when button is pressed
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Set the predicate for looting redberries and cadava
                if(reds.isSelected() && cads.isSelected()){
                    Settings.BUSH = b -> b.getId() == 23625
                            || b.getId() == 23626
                            || b.getId() == 23628
                            || b.getId() == 23629;
                }
                else if(cads.isSelected()){
                    //Set the predicate to only loot cadava bushes
                    Settings.BUSH = b -> b.getId() == 23625
                            || b.getId() == 23626;
                }
                else if(reds.isSelected()){
                    //Set the predicate to only loot redberry bushes
                    Settings.BUSH = b ->  b.getId() == 23628
                            || b.getId() == 23629;
                }
                else{
                    //None selected, defaults to both redberry and cadava
                }
                //Set start to true so the tasks can return true
                Settings.start = true;
                setVisible(false);
            }
        });
    }

    /** USED FOR TESTING IN IDE & DEV
    public static void main(String... args){
        new GUI().setVisible(true);
    }
     */
}
